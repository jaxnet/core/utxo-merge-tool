module tx-merge-tool

go 1.17

require (
	github.com/urfave/cli/v2 v2.2.0
	gitlab.com/jaxnet/jaxnetd v0.4.7
	go.etcd.io/bbolt v1.3.6
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/klauspost/cpuid/v2 v2.0.4 // indirect
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/zerolog v1.25.0 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
