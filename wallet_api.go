package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type BalanceResp struct {
	Data struct {
		Shards []Balance `json:"shards"`
	} `json:"data"`
}

type Balance struct {
	Balance       int64  `json:"balance"`
	BalanceLocked int64  `json:"balanceLocked"`
	ShardID       uint32 `json:"shardID"`
}

type UTXOListResp struct {
	UTXOs []UTXO `json:"utxos"`
	Fees  Fees   `json:"fees"`
}

type UTXO struct {
	Hash     string `json:"hash"`
	Output   int    `json:"output"`
	Amount   int64  `json:"amount"`
	PkScript string `json:"pkScript"`
}

type Fees struct {
	Slow     Fee `json:"fee"`
	Moderate Fee `json:"moderate"`
	Fast     Fee `json:"fast"`
}

type Fee struct {
	Fee       int64 `json:"fee"`
	MaxAmount int64 `json:"maxAmount"`
}

func getBalance(shardID uint32, address string) (*Balance, error) {
	urlAddress := fmt.Sprintf("https://api.peacewallet.io/api/v1/addresses/%s/balances/", address)
	resp, err := http.Get(urlAddress)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status Code = %d (%s)", resp.StatusCode, resp.Status)
	}

	data := new(BalanceResp)
	err = json.NewDecoder(resp.Body).Decode(data)
	if err != nil {
		return nil, err
	}

	for _, balance := range data.Data.Shards {
		if balance.ShardID == shardID {
			return &balance, nil
		}
	}

	return nil, errors.New("Balance not found")
}

func getUTXOs(shardID uint32, address string, amount int64) ([]UTXORow, *Fees, error) {
	urlAddress := fmt.Sprintf("https://api.peacewallet.io/api/v1/shards/%d/addresses/%s/utxos/?amount=%d",
		shardID, address, amount)
	resp, err := http.Get(urlAddress)
	if err != nil {
		return nil, nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, nil, fmt.Errorf("Status Code = %d (%s)", resp.StatusCode, resp.Status)
	}

	type utxoListResp struct {
		Data UTXOListResp `json:"data"`
	}

	data := new(utxoListResp)
	err = json.NewDecoder(resp.Body).Decode(data)
	if err != nil {
		return nil, nil, err
	}

	txHashes := make([]string, len(data.Data.UTXOs))
	for i, utxo := range data.Data.UTXOs {
		txHashes[i] = utxo.Hash
	}

	//	txs, err := getTXs(shardID, txHashes)
	//if err != nil {
	//	return nil, nil, fmt.Errorf("can not get txs for UTXOs: %v", err)
	//}

	utxos := make([]UTXORow, len(data.Data.UTXOs))
	for i := range data.Data.UTXOs {
		//	txHash := data.Data.UTXOs[i].Hash
		utxos[i] = UTXORow{
			UTXO: data.Data.UTXOs[i],
			//			TxData: txs[txHash],
		}
	}

	return utxos, &data.Data.Fees, nil
}

func getTXs(shardID uint32, hashes []string) (map[string]string, error) {
	urlAddress := fmt.Sprintf("https://api.peacewallet.io/api/v1/shards/%d/transactions/", shardID)

	buf := bytes.NewBuffer(nil)
	err := json.NewEncoder(buf).Encode(map[string][]string{
		"hashes": hashes,
	})
	if err != nil {
		return nil, fmt.Errorf("can't encode request body: %v", err)
	}

	resp, err := http.Post(urlAddress, "application/json", buf)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status Code = %d (%s)", resp.StatusCode, resp.Status)
	}

	type respData struct {
		Data map[string]string `json:"data"`
	}

	data := new(respData)
	err = json.NewDecoder(resp.Body).Decode(data)
	if err != nil {
		return nil, err
	}

	return data.Data, nil
}
