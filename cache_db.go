package main

import (
	"log"

	bolt "go.etcd.io/bbolt"
	"sync"
)

var ()

type UTXORow struct {
	UTXO
	UsedInPSBT bool   `json:"usedInPSBT"`
	PSBT       int    `json:"psbt"`
	TxData     string `json:"txData"`
}

// utxoProvider stores local cache of the UTXOs for address.
//
// DB Schema:
//   bucket               key     value
//   <address:shard_id>  "index"  IndexRow
//   <address:shard_id>  "psbt"   []PSBTRow
//   <address:shard_id>   1..N    UTXORow
type utxoProvider struct {
	rw sync.RWMutex

	dbPath string
	db     *bolt.DB
	sender string
}

func (state *utxoProvider) loadIndex(path string) {
	state.rw.Lock()
	defer state.rw.Unlock()

	state.dbPath = path
	var err error
	state.db, err = bolt.Open(state.dbPath, 0600, nil)
	if err != nil {
		log.Fatal("FAILED to open db:", err)
	}
}

func (state *utxoProvider) saveIndex() {
	state.rw.RLock()
	defer state.rw.RUnlock()

	err := state.db.Update(func(tx *bolt.Tx) error {
		return nil
	})
	if err != nil {
		log.Println("FAILED to save utxo to db:", err)
	}
}
