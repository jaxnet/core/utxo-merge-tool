# utxo-merge-tool

This tool combines many of small UTXO into one new UTXO.

> ! Automatic signing and submission are not available now! This feature will be added soon.

To do this, run the program with a correctly set argument. 
It will collect UTXO from the network and combine them in a new PSBT (unsigned transaction).

You have to sign this PSBT (for example, using Electrum Wallet) and submit it by the [helper page](https://explore.jax.net/utils/publish-transaction).

## How to

> prerequisites: install [git](https://git-scm.com/) and [go](https://go.dev/) first.


``` shell
# Get a repo with source code 
git clone https://gitlab.com/jaxnet/core/utxo-merge-tool.git
cd utxo-merge-tool

# Build a binary
go build .

# Get a help & usage 
./utxo-merge-tool -h
```



### Program Help 

``` text
NAME:
   utxo-merge-tool - Combines many small UTXO into one new.

USAGE:
   utxo-merge-tool [global options] command [command options] [arguments...]

Creates one or more PSBTs to be signed and submitted to JAX.Network.
This should be done manually.

usage example:
  ./utxo-merge-tool --shard-id 1 --amount 2000 --address 1FcEkRo9kUYrQgUT3A1whtzR8epTgxFYt7

  # this command will collect UTXOs
  # of the address <1FcEkRo9kUYrQgUT3A1whtzR8epTgxFYt7> for 2000 JUROs in shard 1,
  # As a result you will get hex encoded PSBT


COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --testnet         use tesnet environment instead of the mainnet (default: false)
   --address value   corresponing address of UTXOs
   --shard-id value  id of the corresponding shard, use 0 for beacon (default: 0)
   --limit value     number of inputs per tx (default: 200)
   --amount value    desired amount to merge, must be in JURO. (default: 200*833 JURO, 200*0.0833 JAX) 
   --verbose         show more detailed output (default: false)
   --help, -h        show help (default: false)

```



### Example 

``` shell
./utxo-merge-tool --verbose --shard-id 1 --amount 2000 --address 1FcEkRo9kUYrQgUT3A1whtzR8epTgxFYt7

```

``` shell
Collecting data...

Limit of inputs per tx: 200
Address:  1FcEkRo9kUYrQgUT3A1whtzR8epTgxFYt7

Balance in shard chain 1:
	Balance:	6083399
	LockendBalance:	42483

	Amount to Merge:	2000
	Amount per PSBT:	166600
	Number of PSBT:	1

Creating PSBT #1:
-> Got 3 UTXOs for amount: 2000, fee for tx: 399
-> ... UTXO[0]:	TX(6fafd2c1e7ffa76ef272f8fb531c707d73057b2f99032a6ddc9b969379e35df4)	OUT(1)	Value(833)
-> ... UTXO[1]:	TX(5a7f96cef956b500b67a8937d5ae04113c726d47eace4fafe3fe69dd84ecbcbf)	OUT(1)	Value(833)
-> ... UTXO[2]:	TX(bce7266577607216fdf5de6854c28ad173699ff019f0f66c5e8e173dc73d0dea)	OUT(1)	Value(833)

PSBT[1]:
>>> 70736274ff0100a70100000003f45de37993969bdc6d2a03992f7b05737d701c53fbf872f26ea7ffe7c1d2af6f0100000000ffffffffbfbcec84dd69fee3af4fceea476d723c1104aed537897ab600b556f9ce967f5a0100000000ffffffffea0d3dc73d178e5e6cf6f019f09f6973d18ac25468def5fd167260776526e7bc0100000000ffffffff0141060000000000001976a914a03bcbe85b055be0e83cceae8297f208aaa1a70688ac000000000001041976a914a03bcbe85b055be0e83cceae8297f208aaa1a70688ac0001041976a914a03bcbe85b055be0e83cceae8297f208aaa1a70688ac0001041976a914a03bcbe85b055be0e83cceae8297f208aaa1a70688ac0000
------

```

