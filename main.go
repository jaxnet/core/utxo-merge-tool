package main

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/psbt"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const usage = `utxo-merge-tool [global options] command [command options] [arguments...]

Creates one or more PSBTs to be signed and submitted to JAX.Network.
This should be done manually.

usage example:
  ./utxo-merge-tool --shard-id 1 --amount 2000 --address 1FcEkRo9kUYrQgUT3A1whtzR8epTgxFYt7

  # this command will collect UTXOs
  # of the address <1FcEkRo9kUYrQgUT3A1whtzR8epTgxFYt7> for 2000 JUROs in shard 1,
  # As a result you will get hex encoded PSBT
`

func main() {
	app := &cli.App{
		Name:      "utxo-merge-tool",
		Usage:     "Combines many small UTXO into one new.",
		UsageText: usage,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:  "testnet",
				Usage: "use tesnet environment instead of the mainnet",
			},
			&cli.StringFlag{
				Name:  "address",
				Usage: "corresponing address of UTXOs",
			},
			&cli.Uint64Flag{
				Name:  "shard-id",
				Usage: "id of the corresponding shard, use 0 for beacon",
			},
			// &cli.BoolFlag{
			//	Name:  "sign-n-send",
			//	Usage: "if not set, tool will generate PSBT",
			// },
			&cli.IntFlag{
				Name:  "limit",
				Usage: "number of inputs per tx",
				Value: 200,
			},
			&cli.Int64Flag{
				Name:  "amount",
				Usage: "desired amount to merge, must be in JURO. Default: 200*833 JURO, 200*0.0833 JAX",
			},
			&cli.BoolFlag{
				Name:  "verbose",
				Usage: "show more detailed output",
			},
		},
		Action: createPSBTs,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func createPSBTs(c *cli.Context) error {
	testnet := c.Bool("testnet")
	verbose := c.Bool("verbose")
	shardID := c.Uint64("shard-id")
	limit := c.Int("limit")
	address := c.String("address")
	amount := c.Int64("amount")

	net := &chaincfg.MainNetParams

	if testnet {
		net = &chaincfg.TestNetParams
	}

	jaxAddress, err := jaxutil.DecodeAddress(address, net)
	if err != nil {
		log.Println("ERROR: invalid address format", err)
		return cli.NewExitError("invalid input", 1)
	}

	balance, err := getBalance(uint32(shardID), address)
	if err != nil {
		log.Println("ERROR: can't get balance for addres", err)
		return cli.NewExitError("fatal error", 1)
	}

	fmt.Println("Collecting data...")
	fmt.Println("")
	fmt.Println("Limit of inputs per tx:", limit)
	fmt.Println("Address: ", address)
	fmt.Println("")
	fmt.Printf("Balance in shard chain %d:\n", shardID)
	fmt.Printf("\tBalance:\t%d\n\tLockendBalance:\t%d\n", balance.Balance, balance.BalanceLocked)
	fmt.Println("")

	if amount == 0 {
		amount = int64(limit) * currentShardReward
	}

	maxPerPSBT := int64(limit) * currentShardReward
	psbtCount := (amount / maxPerPSBT) + 1

	fmt.Printf("\tAmount to Merge:\t%d\n\tAmount per PSBT:\t%d\n\tNumber of PSBT:\t%d\n",
		amount, maxPerPSBT, psbtCount)

	psbts := make([]string, psbtCount)
	for n := int64(0); n < psbtCount; n++ {
		fmt.Println("")
		fmt.Printf("Creating PSBT #%d:\n", n+1)

		utxos, fee, err := getUTXOs(uint32(shardID), address, int64(amount))
		if err != nil {
			log.Println("ERROR: unable to collect UTXO for iteration", err)
			return cli.NewExitError("fatal error", 1)
		}
		fmt.Printf("-> Got %d UTXOs for amount: %d, fee for tx: %d\n",
			len(utxos), amount, fee.Moderate.Fee)

		if verbose {
			for i, utxo := range utxos {
				fmt.Printf("-> ... UTXO[%d]:\tTX(%s)\tOUT(%d)\tValue(%d)\n", i, utxo.Hash, utxo.Output, utxo.Amount)
			}
		}

		newPsbt, err := createPSBTObj(jaxAddress, int64(amount), utxos, fee)
		if err != nil {
			log.Println("ERROR: unable to create PSBT", err)
			return cli.NewExitError("fatal error", 1)
		}
		buf := bytes.NewBuffer(nil)
		err = newPsbt.Upsbt.Serialize(buf)
		if err != nil {
			log.Println("ERROR: unable to serialize PSBT", err)
			return cli.NewExitError("fatal error", 1)
		}

		psbts[n] = hex.EncodeToString(buf.Bytes())
	}

	for n, psbtData := range psbts {
		fmt.Printf("\nPSBT[%d]:\n>>> %s\n------\n", n+1, psbtData)
	}

	return nil
}

const currentShardReward = 833 // in Juro; 0.0833 JAX

func createPSBTObj(address jaxutil.Address, amount int64, utxos []UTXORow, fee *Fees) (*psbt.Updater, error) {
	tx := wire.NewMsgTx(1)
	redeemScripts := make([][]byte, len(utxos))

	for i, utxo := range utxos {
		hash, _ := chainhash.NewHashFromStr(utxo.Hash)
		outPoint := wire.NewOutPoint(hash, uint32(utxo.Output))
		tx.AddTxIn(wire.NewTxIn(outPoint, nil, nil))

		redeem, _ := hex.DecodeString(utxo.PkScript)
		redeemScripts[i] = redeem
	}

	value := int64(amount) - fee.Moderate.Fee
	pkScript, _ := txscript.PayToAddrScript(address)
	tx.AddTxOut(wire.NewTxOut(value, pkScript))

	psbtPacket, err := psbt.NewFromUnsignedTx(tx)
	if err != nil {
		return nil, fmt.Errorf("failed to create a psbt packet: %v", err)
	}

	uPsbt, err := psbt.NewUpdater(psbtPacket)
	if err != nil {
		return nil, fmt.Errorf("failed to create an updater: %v", err)
	}

	for i := range redeemScripts {
		err = uPsbt.AddInRedeemScript(redeemScripts[i], i)
		if err != nil {
			return nil, fmt.Errorf("unable to add redeem script(%d): %v", i, err)
		}
	}

	return uPsbt, nil
}
